package main

import (
	"github.com/go-telegram-bot-api/telegram-bot-api"
	"log"
)

type MenuItem string
type MenuItemValue string

const (
	MENU_MAIN               MenuItem = "MAIN MENU"
	MENU_ITEM_SPEAKERS      MenuItem = "SPEAKERS"
	MENU_ITEM_SCHEDULE      MenuItem = "SCHEDULE"
	MENU_ITEM_MASTERS       MenuItem = "MASTERS"
	MENU_ITEM_REGISTRATION  MenuItem = "REGISTRATION"
	MENU_ITEM_FOOD          MenuItem = "FOOD"
	MENU_ITEM_RESETTLEMENT  MenuItem = "RESETTLEMENT"
	MENU_ITEM_SPORT_RESULTS MenuItem = "SPORT RESULTS"
)

var mainMenu = map[MenuItem]MenuItemValue{
	MENU_ITEM_REGISTRATION:  "📝 Регистрация",
	MENU_ITEM_SPEAKERS:      "📢 Спикеры",
	MENU_ITEM_SCHEDULE:      "📅 Расписание",
	MENU_ITEM_MASTERS:       "🎯 Мастер-классы",
	MENU_ITEM_RESETTLEMENT:  "🎒 Расселение",
	MENU_ITEM_SPORT_RESULTS: "🏆 Результаты соревнований",
	MENU_ITEM_FOOD:          "🍏 Где можно перекусить?",
}

var backMenuItems = map[MenuItem]MenuItemValue{
	MENU_ITEM_SPEAKERS: "📢 К списку спикеров",
	MENU_MAIN:          "🏠 В главное меню",
}

const (
	SCHEDULE_FIRST_DAY  = ">>> 29 апреля, Четверг <<<"
	SCHEDULE_SECOND_DAY = ">>> 30 апреля, Пятница <<<"
	SCHEDULE_THIRD_DAY  = ">>> 01 мая, Суббота <<<"
)

var scheduleIndexes = map[int]string{
	0: SCHEDULE_FIRST_DAY,
	1: SCHEDULE_SECOND_DAY,
	2: SCHEDULE_THIRD_DAY,
}

var schedule = map[string]string{
	SCHEDULE_FIRST_DAY: "10:00 - зона для общения и настольных игр\n" +
		"12:00 - 📝 регистрация\n" +
		"12:00 - 🏁 спорт, товарищеские игры\n" +
		"15:00 - 👣 экскурсия по Екатеринбургу\n" +
		"17:00 - 🙏 молитвенная и пророческая комнат UralHop \n" +
		"19:00 - 🙋 открытие конференции\n" +
		"21:00 - ⚽ спорт. Футбол.",
	SCHEDULE_SECOND_DAY: "09:00 - 🏀 спорт. Баскетбол\n" +
		"09:30 - 🍳 завтрак молодежных пасторов (По предварительной записи)\n" +
		"10:00 - 🙌 Уральский дом молитвы UralHop: поклонение словом и ходатайство / пророческая комната (по предварительной записи) \n" +
		"12:00 - ⛪ дневное служение \n" +
		"14:00 - 🎮 спорт. Баскетбол, настольный теннис, шахматы, киберспорт 15:00 - Битва умов \n" +
		"15:00 - 🙌 Уральский дом молитвы: ходатайственный сет / пророческая комната\n" +
		"17:00 - 🎯 мастер-классы\n" +
		"19:00 - ⛪ вечернее служение\n" +
		"21-00 - ⚽ спорт. Футбол.",
	SCHEDULE_THIRD_DAY: "08:30 - 🏆 спорт. Волейбол\n" +
		"10:00 - 🙌 Уральский дом молитвы UralHop: поклонение словом / пророческая комната (по предварительной записи\n" +
		"12:00 - ⛪ утреннее служение\n" +
		"14:00 - 🎸 квартирник\n" +
		"14:00 - 🎮 спорт. Баскетбол, настольный теннис, шахматы, киберспорт\n" +
		"15:00 - 🙌 Уральский дом молитвы UralHop: ходатайственный сет / пророческая комната \n" +
		"17:00 - 🎯 мастер-классы\n" +
		"9:00  - ⛪ вечернее служение, Виктор Судаков \n" +
		"21:00 - ⚽ спорт. Футбол.",
}

type Speaker struct {
	Name        MenuItemValue
	Description string
	Link        string
}

var speakers = []Speaker{
	{Name: "Евгений Зимин"},
	{Name: "Олег Казанцев"},
	{Name: "Виктор Судаков"},
}

func main() {
	bot, err := tgbotapi.NewBotAPI("1671414730:AAEvFVSFKMSt5qjiNGvE9OgPItsHYdgWuxI")
	if err != nil {
		log.Panic(err)
	}

	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message == nil || update.Message.Text == "" { // ignore any non-Message Updates
			continue
		}

		log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)

		if update.Message.Text[0:1] == "/" {
			switch update.Message.Text {
			default:
				sendMainMenu(bot, update)
			}
		} else {
			switch MenuItemValue(update.Message.Text) {
			case backMenuItems[MENU_MAIN]:
				sendMainMenu(bot, update)

			// Главное меню
			case mainMenu[MENU_ITEM_SPEAKERS]:
				sendSpeakersMenu(bot, update)
			case mainMenu[MENU_ITEM_SCHEDULE]:
				//var msg string
				//for i := 0; i < len(schedule); i++ {
				//	msg += scheduleIndexes[i] + "\n" + schedule[scheduleIndexes[i]] + "\n\n"
				//}
				sendTextResponse(bot, update.Message.Chat.ID, "https://telegra.ph/Raspisanie-konferencii-04-26")
			case mainMenu[MENU_ITEM_RESETTLEMENT]:
				rspMsg := "В этом году из-за эпидемиологической ситуации не будет расселения в домах верующих, поэтому мы предлагаем подборку недорогих хостелов в шаговой доступности от церкви!\nНадеемся, что вы сможете найти подходящий для вас вариант\nХостелы:\nhttps://ahostel.ru/\nhttp://www.aurorah.ru/\nhttps://hostel-ekaterinburg.ru/\nhttp://ekat.london/ru/\nhttps://roomy-hostel.com/\nhttps://www.ekb-hostel.ru/"
				sendTextResponse(bot, update.Message.Chat.ID, rspMsg)
			case mainMenu[MENU_ITEM_REGISTRATION]:
				rspMsg := "Пожалуйста, регистрируйтесь здесь:\nhttps://forms.gle/kA1sz2LRWwSFwexP6\n Если ещё не зарегистрировались, убедительно просим вас сделать это :)"
				sendTextResponse(bot, update.Message.Chat.ID, rspMsg)
			case mainMenu[MENU_ITEM_SPORT_RESULTS]:
				rspMsg := "Результаты соревнований вы можете посмотреть здесь: https://docs.google.com/document/d/16ujnKiu4JXBh_XL8YuDEK3wvYKCUNzSYdBZ-4hNykvU"
				sendTextResponse(bot, update.Message.Chat.ID, rspMsg)
			case mainMenu[MENU_ITEM_FOOD]:
				rspMsg := "https://telegra.ph/Gde-mozhno-perekusit-04-26"
				sendTextResponse(bot, update.Message.Chat.ID, rspMsg)
			case mainMenu[MENU_ITEM_MASTERS]:
				rspMsg := "https://telegra.ph/Master-klassy-eshche-na-redakcii-04-26"
				sendTextResponse(bot, update.Message.Chat.ID, rspMsg)

			// Спикеры
			case speakers[0].Name:
				sendPhotoResponse(bot, update.Message.Chat.ID, "AgACAgIAAxkBAAPQYHaL7_ybmCflH7z7UqQLbmCwj8EAAtuwMRuYbblLs3G1amJ6MdNbxtKaLgADAQADAgADeAADzEcFAAEfBA")
				rspMsg := "🌟СПИКЕР #RELOAD21\nЕвгений Зимин - Детский и подростковый лидер церкви \"Новая Жизнь\" г. Екатеринбург, организатор детских христианских лагерей и семейных выездов, уже много лет служит детям и подросткам нашей церкви и мы точно знаем, что у него есть важное и актуальное послание для молодежи!"
				sendTextResponse(bot, update.Message.Chat.ID, rspMsg)
			case speakers[1].Name:
				sendPhotoResponse(bot, update.Message.Chat.ID, "AgACAgIAAxkBAAPUYHaMkQABdYkPgBlaB7v1xavEZWHSAALesDEbmG25SymMTVJkfrvWSEzGoi4AAwEAAwIAA3kAA3stAQABHwQ")
				rspMsg := "🌟СПИКЕР #RELOAD21\nЛидер прославления церкви \"Новая Жизнь\", г. Екатеринбург, основатель Уральского дома молитвы и поклонения, автор песен. Человек, влюблённый в Бога, с огнём внутри, прекрасным чувством юмора и с совершенным голосом"
				sendTextResponse(bot, update.Message.Chat.ID, rspMsg)
			case speakers[2].Name:
				sendPhotoResponse(bot, update.Message.Chat.ID, "AgACAgIAAxkBAAPVYHaNFk6PUbdbvbkLtAZJ_kL-5xcAAt-wMRuYbblLpMj_zrPP2OKL5E2eLgADAQADAgADeQADpUMFAAEfBA")
				rspMsg := "🌟СПИКЕР #RELOAD21\nНаш дорогой и любимый Пастор церкви \"Новая Жизнь\" г. Екатеринбург, автор христианских книг и телепередач. \nЕпископ РОСХВЕ по Свердловской области. Отзывчивый служитель с огромным сердцем, пасторский путь которого направлен на то, чтобы влюблять людей в Господа.\nМы очень ценим пастора за то, что его сердце открыто для молодежи и его проповеди вдохновляют и заряжают энергией❤"
				sendTextResponse(bot, update.Message.Chat.ID, rspMsg)
			}
		}
	}
}

func getMenu(menuList []string) tgbotapi.ReplyKeyboardMarkup {
	var rows [][]tgbotapi.KeyboardButton
	for _, menuItem := range menuList {
		rows = append(rows, tgbotapi.NewKeyboardButtonRow(
			tgbotapi.NewKeyboardButton(menuItem),
		))
	}

	return tgbotapi.NewReplyKeyboard(rows...)
}

func sendMenuResponse(bot *tgbotapi.BotAPI, chatID int64, msgText string, menu tgbotapi.ReplyKeyboardMarkup) {
	msg := tgbotapi.NewMessage(chatID, msgText)
	msg.ReplyMarkup = menu
	bot.Send(msg)
}

func sendTextResponse(bot *tgbotapi.BotAPI, chatID int64, msgText string) {
	msg := tgbotapi.NewMessage(chatID, msgText)
	bot.Send(msg)
}

func sendPhotoResponse(bot *tgbotapi.BotAPI, chatID int64, fileId string) {
	msg := tgbotapi.NewPhotoShare(chatID, fileId)
	bot.Send(msg)
}

func sendMainMenu(bot *tgbotapi.BotAPI, update tgbotapi.Update) {
	rspMsg := "Привет! Мы рады приветствовать тебя на нашей ежегодной молодёжной конференции Reload!\n Здесь ты найдёшь всю необходимую информацию."

	menuItems := make([]string, 0, len(mainMenu))
	for _, menuItem := range mainMenu {
		menuItems = append(menuItems, string(menuItem))
	}
	sticker := tgbotapi.NewStickerShare(update.Message.Chat.ID, "CAACAgIAAxkBAAOhYG6bgBP3ELPAl5V0Xw7YTV1uQNkAAgEBAAJWnb0KIr6fDrjC5jQeBA")
	bot.Send(sticker)
	sendMenuResponse(bot, update.Message.Chat.ID, rspMsg, getMenu(menuItems))
}

func sendSpeakersMenu(bot *tgbotapi.BotAPI, update tgbotapi.Update) {
	rspMsg := "Наши прекрасные спикеры"

	var rows [][]tgbotapi.KeyboardButton

	rows = append(rows, tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton(string(backMenuItems[MENU_MAIN])),
	))

	for _, speaker := range speakers {
		rows = append(rows, tgbotapi.NewKeyboardButtonRow(
			tgbotapi.NewKeyboardButton(string(speaker.Name)),
		))
	}

	menu := tgbotapi.NewReplyKeyboard(rows...)
	sendMenuResponse(bot, update.Message.Chat.ID, rspMsg, menu)
}
